package com.an22.main.ui.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.an22.main.R
import com.an22.main.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {
    var handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_page)

        val runnable = Runnable {
            intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        handler.postDelayed(runnable, 3000)
    }
}